import React from 'react'
import {render} from 'react-dom'
// import {Router} from 'react-router'
import { Router, Route, Link, browserHistory } from 'react-router'
// import { useRouterHistory } from 'react-router'
// import { createHashHistory } from 'history'
// import { createBrowserHistory } from 'history'

import History from '../components/layout/navigation/classes/History.js';

import Routes from './Routes.jsx';

// const appHistory = useRouterHistory(createHashHistory)({ queryKey: false })

var rootInstance = render((
    <Router history={browserHistory}>
        {Routes}
    </Router>
), document.getElementById('smartadmin-root'));

if (module.hot) {
    require('react-hot-loader/Injection').RootInstanceProvider.injectProvider({
        getRootInstances: function () {
            // Help React Hot Loader figure out the root component instances on the page:
            return [rootInstance];
        }
    });
}
